package com.example.signin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var fg_pw : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fg_pw = findViewById(R.id.fg_pw)
    }
    public fun onClickSignIn(view: View){
        if(fg_pw.visibility == View.VISIBLE){
            fg_pw.visibility = View.GONE
        }
        else{
            fg_pw.visibility = View.VISIBLE
        }
    }
}