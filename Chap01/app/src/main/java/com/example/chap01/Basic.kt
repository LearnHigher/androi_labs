package com.example.chap01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class Basic : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basic)
////////////////////////////////// CACH KHAI BAO BIEN //////////////////////////////////
//        // var: khai bao 1 bien co the thay doi duoc
//        var a : String = ""
//        a = "Hung"
//
//        // val: khai bao 1 bien khong the thay doi duoc
//        val b : String = "Ta Thanh"
////        b = "Ten Khac" // Lap tuc bao loi

////////////////////////////////// EP KIEU DU LIEU //////////////////////////////////

//        a.toInt()

////////////////////////////////// CONDITIONS //////////////////////////////////

        // IF - ELSE
        var a = 10
        var b = 29

        if (a > b){
            Log.d("AAA", "Gía trị lớn hơn là: " + a)
            // Nhập AAA để sau khi run chương trình ta sẽ kiểm tra dòng code trong log cart
        }
        else{
            Log.d("AAA", "Gía trị lớn hơn là: " + b)
        }

////////////////////////////////// Class and object //////////////////////////////////

    }
}