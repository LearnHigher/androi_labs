package com.example.signupaccount

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.MovementMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.klinker.android.link_builder.Link
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mIsShowPass = false
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showHidePass.setOnClickListener{
            mIsShowPass = !mIsShowPass
            showPassword(mIsShowPass)
        }
        showPassword(mIsShowPass)
        setLink()
        validationForm()
        startLogInActive(validationForm())
    }

    //Set show/hide password
    private fun showPassword(isShow: Boolean){
        if(isShow){
            enterPass.transformationMethod = HideReturnsTransformationMethod.getInstance()
            showHidePass.setImageResource(R.drawable.ic_show_password)
        }
        else{
            enterPass.transformationMethod = PasswordTransformationMethod.getInstance()
            showHidePass.setImageResource(R.drawable.hide_password)
        }
        enterPass.setSelection(enterPass.text.toString().length)
    }

    //Set the link to website
    private fun setLink(){

        val textView: TextView = findViewById(R.id.linkText)
        textView.movementMethod = LinkMovementMethod.getInstance()

    }

    fun validationForm(): Boolean{

        var btnActivate : Boolean = true

        var mail    : String   =   email.text.toString()
        var name    : String   =   userName.text.toString()

        if (mail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
            email.setError("enter a valid email address");
            btnActivate = false;
        } else {
            email.setError(null);
        }

        if (name.isEmpty()) {
            userName.setError("You must have a name");
            btnActivate = false;
        } else {
            userName.setError(null);
        }

        return btnActivate
    }
    fun startLogInActive( btnActive : Boolean){
        start_login.isEnabled = btnActive == true
    }
}


