package com.example.chatuitemplate

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

import java.util.*

class MainActivity : AppCompatActivity() {

    // create relative-recyclerview object (recyclerView)
    val arrayList = java.util.ArrayList<Friends>()
    val displayList = java.util.ArrayList<Friends>()
    lateinit var myAdapter: MyAdapter
    lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        arrayList.add(Friends("Robert Ronadltion", "Mai đi chơi nha", R.drawable.avatar_1))
        arrayList.add(Friends("Janiefer Lauren", "Cảm ơn bạn nha", R.drawable.avatar_2))
        arrayList.add(Friends("Thomas Edition", "Phát minh này hay nè", R.drawable.avatar_3))
        arrayList.add(Friends("Daniel Network", "Xem  thử phim Fate/Stay night đi, hay lắm", R.drawable.avatar_4))
        arrayList.add(Friends("Sếp", "Xấp tài liệu chuẩn bị xong chưa?", R.drawable.avatar_5))
        arrayList.add(Friends("Emiya Shirou", "Tuần sau tớ bận rồi", R.drawable.avatar_6))
        arrayList.add(Friends("Janne Azziar", "Ok bạn", R.drawable.avatar_7))

        arrayList.add(Friends("Rob Lane", "Solo violin không?", R.drawable.avatar_1))
        arrayList.add(Friends("Kelvin Inventor", "No problem", R.drawable.avatar_2))
        arrayList.add(Friends("Sandy Fine", "Phát minh này hay nè", R.drawable.avatar_3))
        arrayList.add(Friends("Daniel Goid", "Xem  thử phim Fate/Stay night đi, hay lắm", R.drawable.avatar_4))
        arrayList.add(Friends("NPC", "T4 đi xem phim không?", R.drawable.avatar_5))
        arrayList.add(Friends("Emiya Kiristu", "Tuần sau tớ bận rồi", R.drawable.avatar_6))
        arrayList.add(Friends("Louis the best", "Ok bạn", R.drawable.avatar_7))

        displayList.addAll(arrayList)

        myAdapter = MyAdapter(displayList, this)

        recyclerView.layoutManager  =   LinearLayoutManager(this)
        recyclerView.adapter = myAdapter

        preferences =   getSharedPreferences("My_Pref", Context.MODE_PRIVATE)
        val mSortSetting    =   preferences.getString("Sort", "A -> Z")
        if (mSortSetting == "A -> Z"){
            sortAsending(myAdapter)
        }
        else if (mSortSetting == "Z <- A"){
            sortDesending(myAdapter)
        }
    }
    private fun sortDesending(myAdapter: MyAdapter) {
        displayList.sortWith(compareBy {it.name})
        displayList.reverse()
        myAdapter.notifyDataSetChanged()
    }

    private fun sortAsending(myAdapter: MyAdapter) {
        displayList.sortWith(compareBy {it.name})
        myAdapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu, menu)
        val menuItem = menu!!.findItem(R.id.search)

        if (menuItem != null){
            val searchView = menuItem.actionView as SearchView
            val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            editText.hint = "Search ..."

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (newText!!.isNotEmpty()){
                        displayList.clear()
                        val search = newText.toLowerCase(Locale.getDefault())
                        arrayList.forEach{
                            if (it.name.toLowerCase(Locale.getDefault()).contains(search)){
                                displayList.add(it)
                            }
                        }
                        recyclerView.adapter!!.notifyDataSetChanged()
                    }
                    else{
                        displayList.clear()
                        displayList.addAll(arrayList)
                        recyclerView.adapter!!.notifyDataSetChanged()
                    }
                    return true
                }

            })
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id  =   item.itemId
        if (id == R.id.sorting){
            sortDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun sortDialog() {
        val options = arrayOf("A -> Z", "Z <- A")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Sort by")
        builder.setIcon(R.drawable.ic_action_sort)
        builder.setItems(options){dialog, which ->
            if (which == 0 ){
                val editor : SharedPreferences.Editor = preferences.edit()
                editor.putString("Sort", "A -> Z")
                editor.apply()
                sortAsending(myAdapter)
                Toast.makeText(this, "Đã sắp xếp từ A đến Z", Toast.LENGTH_LONG).show()
            }
            if (which == 1 ){
                val editor : SharedPreferences.Editor = preferences.edit()
                editor.putString("Sort", "Z -> A")
                editor.apply()
                sortDesending(myAdapter)
                Toast.makeText(this, "Đã sắp xếp từ Z đến A", Toast.LENGTH_LONG).show()
            }
        }
        builder.create().show()
    }
}


