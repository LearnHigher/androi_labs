package com.example.chatuitemplate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import kotlinx.android.synthetic.main.activity_another.*

class AnotherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_another)

        val actionBar:  ActionBar?  =   supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar!!.setDisplayShowHomeEnabled(true)

        // now get data from putExtra intent
        var intent  =   intent
        val aName   =   intent.getStringExtra("iName")
        val aAvatar =   intent.getIntExtra("iAvatar", 0)

        // set name in another activity
        actionBar.setTitle(aName)
        itemName.text   =   aName
        imageView.setImageResource(aAvatar)
    }
}