package com.example.chatuitemplate

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_chat.view.*

class MyAdapter(val arrayList: ArrayList<Friends>, val context: MainActivity) : RecyclerView.Adapter<MyAdapter.ViewHolder>(){
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bindItem(friends: Friends){
            itemView.name.text      =   friends.name
            itemView.message.text   =   friends.message
            itemView.avatar_friend_img.setImageResource(friends.avatar)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(arrayList[position])

        holder.itemView.setOnClickListener{

            // get position of selected item
            val friends = arrayList.get(position)

            // get name and avatar of selected item with intent
            var gName: String   =   friends.name
            var gAvatar: Int    =   friends.avatar

            // create intent in kotlin
            //val intent  =   Intent(context, AnotherActivity::class.java)
            val intent  =   Intent(context, AnotherActivity::class.java)

            // now put all these items with putExtra intent
            intent.putExtra("iName", gName)
            intent.putExtra("iAvatar", gAvatar)

            // Start another activity
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
}