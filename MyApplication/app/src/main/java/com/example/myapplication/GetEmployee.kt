package com.example.myapplication

import android.util.Log
import okhttp3.OkHttpClient
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class GetEmployee:retrofit2.Callback<GetEmployee.Result> {

    data class Result(
        var status :String,
        var data:ArrayList<Employee>
    )

    data class Employee(var id:String, var employee_name:String, var employee_salary:String, var employee_age:String, var profile_image:String)

    interface API{
        @GET("/api/v1/employees")
        fun getEmployee() : Call<Result>
    }

    interface Callback{
        fun complete ( result:Result)
    }
    public fun getEmployee (){
        var okhttpClient=OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30,TimeUnit.SECONDS)
            .build()

        var retrofit = Retrofit.Builder()
            .baseUrl("http://dummy.restapiexample.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okhttpClient).build()

        var api = retrofit.create(API::class.java)
        var call= api.getEmployee()
        call.enqueue(this)
    }

    override fun onResponse(call: Call<Result>, response: Response<Result>) {
       Log.d("xxx",response.body().toString())
       Log.d("xxx",response.body()?.status+"")

        if(response.body()?.status== "success"){
            response.body()?.data?.forEach { e->
                Log.d("xxx",e.toString())
            }
        }

    }

    override fun onFailure(call: Call<Result>, t: Throwable) {
        Log.d("xxx",t.toString())
    }
}