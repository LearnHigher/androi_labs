package com.example.Main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import com.example.Main.fragments.ChatFragment
import com.example.Main.fragments.FriendsFragment
import com.example.Main.fragments.HomeFragment
import com.example.Main.fragments.StoriesFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    lateinit var homeFragment: HomeFragment
    lateinit var friendsFragment: FriendsFragment
    lateinit var chatFragment: ChatFragment
    lateinit var storiesFragment: StoriesFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var bottomnav: BottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        var frame: FrameLayout = findViewById<FrameLayout>(R.id.fragment_container)

    }
}