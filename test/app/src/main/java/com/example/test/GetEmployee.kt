package com.example.test

import okhttp3.OkHttpClient
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class GetEmployee {
    public class Result{
        var status: String? = null
        val data = arrayListOf<Employee>()
    }
    data class Employee(var id: Int,
                        var employee_name: String,
                        var employee_salary: String,
                        var employee_age: String,
                        var profile_image: String){
    }

    interface API{
        @GET("/api/v1/employees")
        fun Employee() : Call<Result>
    }
    fun Employee(){
        var okHttpClient = OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()

        var retrofit = Retrofit.Builder()
            .baseUrl("http://dummy.restapiexample.com")
    }
}