package com.example.recycleview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recycleview.chat_List
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val exampleList = generateDummyList(100)
        chatList.adapter = ListAdapter(exampleList)
        chatList.layoutManager = LinearLayoutManager(this)
        chatList.setHasFixedSize(true)
    }

    private fun generateDummyList(size: Int): List<chat_List>{
        val list = ArrayList<chat_List>()

        for (i in 1 until size){
            val drawable = when (i % 3){
                0 -> R.drawable.ic_android
                1 -> R.drawable.ic_audio
                else -> R.drawable.ic_sun
            }
            val item = chat_List(drawable, "Item $i", "Line $i")
            list += item
        }
        return list
    }
}